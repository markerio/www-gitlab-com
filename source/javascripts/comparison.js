$(function() {
  var $compareTable = $('.compare-table');
  var $tableHolder = $compareTable.closest('.table-responsive');
  var $compareTableTds = $('td', $compareTable);
  var $thead = $('thead', $compareTable);
  var $th = $thead.find('th');
  var $nav = $('.navbar-default');

  $thead.affix({
    offset: {
      top: function() {
        return (this.top = $thead.offset().top - ($nav.outerHeight() + 10));
      }
    }
  }).on('affixed.bs.affix', function() {
    $th.each(function(i) {
      var $this = $(this);
      $this.width($compareTableTds.eq(i).outerWidth());
    });

    $tableHolder.css({
      paddingTop: $thead.height()
    });
  }).on('affixed-top.bs.affix', function() {
    $th.css({
      width: ''
    });

    $tableHolder.css({
      paddingTop: ''
    });
  });

  if ($thead.is('.affix')) {
    $thead.trigger('affixed.bs.affix');
  }

  var CategoriesFilter = {
    selector: '#categories-filter',
    $el: [],
    value: '',
    label: '',
    $title: [],
    init: function() {
      this.$el = $(this.selector);
      if (this.$el.length > 0) {
        this.setup();
      }
    },
    setup: function() {
      this.$title = this.$el.find('.dropdown-menu-toggle span');
      $(document).on('click', this.selector + ' li', this.setValue.bind(this));
    },
    setValue: function(event) {
      var $$ = $(event.currentTarget);
      this.label = $$.text().trim();
      this.value = $$.data('category-key');
      this.render();
      this.$el.trigger('change.comparisonFilter');
    },
    render: function() {
      this.$title.text(this.label);
    }
  };
  CategoriesFilter.init();

  var CompetitorsFilter = {
    selector: '#competitors-filter',
    $el: [],
    $selected_option: null,
    $options: [],
    $title: [],
    default_title: '',
    init: function() {
      this.$el = $(this.selector);
      if (this.$el.length > 0) {
        this.setup();
      }
    },
    setup: function() {
      this.$title = this.$el.find('.dropdown-menu-toggle span');
      this.default_title = this.$title.text().trim();
      this.$options = this.$el.find('.dropdown-menu li');
      this.selectInitial();
      this.render();
      $(document).on('click', this.selector + ' li', this.filterChange.bind(this));
      $(document).on('change.comparisonFilter', CategoriesFilter.selector, this.render.bind(this));
    },
    selectInitial: function() {
      if (this.$el.data('selected')) {
        var $option = this.$options.filter('[data-key="' + this.$el.data('selected') + '"]');
        if ($option.length === 1) {
          this.$selected_option = $option;
        }
      }
    },
    value: function() {
      return this.$selected_option !== null ? this.$selected_option.text().trim() : this.default_title;
    },
    linkValue: function() {
      return this.$selected_option !== null ? this.$selected_option.data('key') : '';
    },
    filterChange: function(event) {
      this.$selected_option = $(event.currentTarget);
      this.render();
      this.$el.trigger('change.comparisonFilter');
    },
    render: function() {
      if (CategoriesFilter.value) {
        var filterSelector = '[data-categories*="||' + CategoriesFilter.value + '||"]';
        this.$options.hide().data('hidden', true).filter(function() {
          return $(this).is(filterSelector);
        }).show().data('hidden', false);
      } else {
        this.$options.show();
      }
      if (this.$selected_option !== null && this.$selected_option.data('hidden')) {
        this.$selected_option = null;
        this.$el.trigger('change.comparisonFilter');
      }
      this.$title.text(this.value());
    }
  };
  CompetitorsFilter.init();

  var GitLabProductsFilter = {
    value: function() {
      return 'GitLab';
    },
    linkValue: function() {
      return 'gitlab';
    }
  };

  var CompareButton = {
    $el: [],
    init: function() {
      this.$el = $('#compare-button');
      if ( this.$el.length > 0 ) {
        this.setup();
      }
    },
    setup: function() {
      this.$el.attr('disabled', true);
      $(document).on('change.comparisonFilter', this.render.bind(this));
    },
    render: function() {
      var gitlabProduct = GitLabProductsFilter.linkValue();
      var competitor = CompetitorsFilter.linkValue();
      if (!gitlabProduct || !competitor) {
        this.$el.attr('disabled', true);
        return;
      }
      this.$el.attr('disabled', false);
      var href = ('/comparison/' + competitor + '-vs-' + gitlabProduct + '.html').replace(/_/g, '-');
      this.$el.attr('href', href);
    }
  };
  CompareButton.init();
});
