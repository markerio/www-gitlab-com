---
layout: markdown_page
title: "Sales Enablement"
---



## Sales Enablement
### Training Playlist
You can watch previous [GitLab sales enablement training sessions](https://www.youtube.com/watch?v=ZyyBq3_rzJo&list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG) on YouTube. Also, here is a link to a [deprecated document](https://docs.google.com/spreadsheets/d/1ETY7FfCzb2q9h2EkYttlW_Qpl7IHUF-F2rOJG2W03Yk/edit#gid=0) with more historical training sessions and recordings. Both direct sales and channel sales training are part of the same playlist. We place the recordings of our training on YouTube so that everyone can access. We love everyone to contribute including:
- Direct Salespeople
- Channel Resellers
- Rest of company
- Customers
- Users
- Analysts
- Partners

### Enablement pages
- [GitLab CI/CD for GitHub FAQ](./github-ci-cd-faq.html)

### Upcoming Training
To see what training is coming soon view the [Sales Enablement issue board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement)

### To request new sales enablement sessions
- Create an issue in the [Marketing general issue](https://gitlab.com/gitlab-com/marketing/general/issues), tracker
- Label the issue with `Sales enablement`
- @mention `@williamchia` in the issue description and he will coordinate with the product marketing team to prioritize and schedule the training.
- If you need a training prioritized @mention `@pmm-team` in the `#product-marketing` slack channel.

### Team Calendar and Enablement
Team Calendar edit access (either you need it or you need to know who does) Because we need to update the enablement invitation with the zoom link before the enablement starts, either you or some on the team needs to have access to the GitLab team calendar to EDIT and update events.  Currently Ashish, John, and Jennifer have edit access. See (Team Meetings)[https://about.gitlab.com/handbook/tools-and-tips/#gitlab-team-meetings-calendar]

## XDR (BDR/SDR) Coaching
In order to provide frequent feedback and coaching to the BDR/SDR team, weekly coaching/enablement sessions are hosted where the XDR team is able to ask questions about specific topics they are encountering with their customers. These coaching sessions are driven based on demand from the team and are interactive to help grow skills, awareness and knowledge about the IT and software delivery challenges that enterprise customers are facing. 

[XDR Coaching Notes/Topics](./xdr-coaching)

### Establishing the XDR Coaching program
#### First meeting - AMA to set the stage and get organized

**Objective:**

* Encourage the team open up and be comfortable asking questions.   
* Establish ongoing agenda and get inputs for specific topics.

#### Proposed Recurring Agenda:
* Pre-work (preparation - specific resource to read, objectives, questions
* 10 min - specific discussion topic  (PMM presents a topic)
* 19 min - interactive / q&a - (where XDRs share case study/situations)
* 1 min -  Next topic (prework/prep for next session)

#### Requesting future discussion topics:
To requst discussion topics and/or to see future topics see the [XDR Coaching issue board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?&label_name[]=XDR-Coaching)

#### Potential Resources:
* (An overview of IT/SW delivery challenges:)[https://learn.techbeacon.com/tracks/business-leaders-guide-software-innovation]
* others tbd


## LiveStream Enablement Setup Instructions
Sales Enablement is recorded in zoom and then the presentation portion is posted on YouTube. The recording is stopped to allow for open questioning during the session. Once the event is live, it can be viewed here on [YouTube GitLab] !(/images/handbook/marketing/product-marketing/Youtube-ViewEvent.png).
