---
layout: markdown_page
title: "Enterprise IT Roles"
---
## Understanding what drives those we sell to

### Enterprise IT & their challenges

* Digital Transformation - IT expected to enable faster delivery of customer led innovation.
* Continuously deliver high quality end user experience - quality with speed.
* Excessive complexity across IT architecture and infrastructure.
Lack of full metrics and visibility across the entire process.
* Compliance - prove compliance with IT controls and industry regulations.
* Security - IT teams are under pressure to reduce risk, prevent and mitigate leaks.
* Culture and collaboration issues in the workplace - legacy organizations and silos.
* Budget constraints limit their options.

Related Reading:

- [10 digital transformation success stories](https://www.cio.com/article/3149977/digital-transformation/digital-transformation-examples.html?nsdr=true#tk.cio_rs)
- [The CIO’s Dilemma: Innovate AND Cut Costs](https://www.cio.com/article/3300871/cloud-computing/the-cio-s-dilemma-innovate-and-cut-costs.html)  
- [Complexity a killer when it comes to digital transformation success](https://www.cio.com/article/3269493/digital-transformation/complexity-a-killer-when-it-comes-to-digital-transformation-success.html) - [5 Big Challenges CIOs face](https://www.mrc-productivity.com/blog/2017/11/5-big-challenges-facing-cios-leaders-2018/)
- [9 forces shaping the future of IT](https://www.cio.com/article/3206770/it-strategy/9-forces-shaping-the-future-of-it.html?upd=1538513299753)
- [12 biggest issues IT faces](https://www.cio.com/article/3245772/it-strategy/the-12-biggest-issues-it-faces-today.html)
- [Survey: Compliance Drives IT Security](https://www.cio.com/article/2447696/compliance/survey--compliance-drives-it-security.html)
- [Financial Services Regulatory Compliance](https://about.gitlab.com/solutions/financial-services-regulatory-compliance/)
- [Collaboration key to achieving business goals](https://www.cio.com/article/3170784/collaboration/collaboration-key-to-achieving-business-goals.html)

### Who cares about what? What pains do they have?

1. **CxO**

    Key initiatives focused on (1) Digital Transformation (2) Moving to the cloud (cloud native) (3) Speed time to market without impacting risk

    Defining key trends and terms:
    * What is digital transformation - Read [Forrester Digital Rewrites the Rules of Business](https://drive.google.com/file/d/16kb47ifUBX5Zp4lAiAbMp7TVo79ba2MY/view) (INTERNAL only)
    * Why is [cloud native](https://about.gitlab.com/cloud-native/) important to digital transformation
    * Speed [time to market](https://about.gitlab.com/solutions/faster-software-delivery/) without impacting [risk](https://about.gitlab.com/solutions/dev-sec-ops/)

        **CIO or VP IT** - Business focus, strategic, long-term, transformation, budget
        1. Persona reference: [VP IT video](https://www.youtube.com/watch?v=LUh5eevH3F4), [VP IT Slide Deck](https://docs.google.com/presentation/d/17Ucpgxzt1jSCs83ER4-LdDyEuermpDuriugPNYrz8Rg/)
        1. Value Prop:  
              - GitLab’s single application enables more rapid development (see GitLab stats as an example) to meet time-to-market business challenges.
              - Unite workflows around a common tool to reduce friction and cost of IT.
              - Reduce or eliminate your costly DevOps tool chain (or avoid this investment in one).
        1. Resources:
              - [GitLab Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
              - [GitLab home page](https://about.gitlab.com)
              - [ROI](https://about.gitlab.com/roi/replace/)
              - [Product page](https://about.gitlab.com/product/)
              - [Comparison page](https://about.gitlab.com/comparison/)
              - [Solutions page](https://about.gitlab.com/solutions/), depending upon unique challenges/interests  


        **CISO and VP Security** - Risk, Security, Compliance, protecting the business
        1. Value Prop:
              - You no longer must choose between velocity and risk.
                  1. With GitLab, you can test ALL of your code, on every commit, automatically                              
                  1. Because it’s not an incremental cost per app or per user (beyond using GitLab Ultimate for your entire SDLC), you can test every code change, not just critical apps or annual scans.  
              - Better leverage your scarce security resources by putting app sec tools, that are meant for the developer, into the hands of the developer, so they may remediate more, earlier than possible with traditional app sec tools. (Enable TRUE shift-left via single application and single source-of-truth.)
              - Improve visibility while at the same time reducing friction between processes and tools used by dev and app sec teams.  
        1. Resources:
              - [DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops)
              - [Comparison](https://about.gitlab.com/comparison/)
              - [Security Deck](https://docs.google.com/presentation/d/1lNr9pz7axLlN7uw7Wkwi_FYMuEh4F4QzPaoJLfReGFk/edit#slide=id.g2823c3f9ca_0_9)
              - [Regulatory Compliance](https://about.gitlab.com/solutions/compliance)  

        **CTO** - Technology focused, today and in the future. Focus can vary from company to company.
        1. Persona reference: [Chief Architect Video](https://www.youtube.com/watch?v=qyELotxsQzY), [Chief Architect slide deck](https://docs.google.com/presentation/d/1KXsozYkimSLlEg3N-sKeN7Muatz_4XimYp-s7_dY1ZM/)
        1. Value Prop:  
              - GitLab’s single application enables more rapid development (see GitLab stats as an example) to meet time-to-market business challenges. It positions your enterprise for maximum flexibility and speed.
              - Unite workflows around a common tool to reduce friction and cost of IT.
              - Reduce or eliminate your costly DevOps tool chain (or avoid this investment in one).
              - Improve visibility while at the same time reducing friction between processes and tools used by dev, ops and app sec teams.
        1. Resources:
            - [GitLab Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
            - [GitLab home page](https://about.gitlab.com)
            - [ROI](https://about.gitlab.com/roi/replace/)
            - [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/)
            - [Cloud Transformation](https://about.gitlab.com/solutions/cloud-native/)
            - [Software-defined Infrastructure](https://about.gitlab.com/solutions/no-ticket-provisioning/)
            - [Visibility and Velocity](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/)
            - [Geographically distributed teams](https://about.gitlab.com/features/gitlab-geo/)
            - [High availability development](https://about.gitlab.com/high-availability/)
            - [Team development platform](https://about.gitlab.com/solutions/developer-platform/)
            - [DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops)
            - [Product page](https://about.gitlab.com/product/)
            - [Comparison page](https://about.gitlab.com/comparison/)
            - [Solutions page](https://about.gitlab.com/solutions/), depending upon unique challenges/interests

1. **VP Level**

      **VP Apps/Development/Engineering** - Helping to meet business demand for innovation, updates, and capability. This role is typically bonused/compensated on velocity, time to market and alignment with the business.
      1. Value Prop
          - GitLab’s single application that supports the entire DevOps lifecycle is of significant importance.  The DevOps tool chain crisis is real and there is a lot of ‘bubble gum and duct tape’ going on in and around integrating all these point DevOps tools in order to convey the full story of what’s really happening.
          - Finding new areas across the SDLC to automate is top of mind to this role.  GitLab’s [Auto DevOps](https://about.gitlab.com/auto-devops/) will be a competitive differentiator
      1. Resources:
          - [GitLab Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
          - [GitLab home page](https://about.gitlab.com)
          - [ROI](https://about.gitlab.com/roi/replace/)
          - [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/)
          - [Software-defined Infrastructure](https://about.gitlab.com/solutions/no-ticket-provisioning/)
          - [Visibility and Velocity](https://about.gitlab.com/2017/06/07/continous-integration-ticketmaster/)
          - [Geographically distributed teams](https://about.gitlab.com/features/gitlab-geo/)
          - [High availability development](https://about.gitlab.com/high-availability/)
          - [Team development platform](https://about.gitlab.com/solutions/developer-platform/)
          - [Auto DevOps](https://about.gitlab.com/auto-devops/)
          - [CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/)
          - [Product page](https://about.gitlab.com/product/)
          - [Comparison page](https://about.gitlab.com/comparison/)
          - [Solutions page](https://about.gitlab.com/solutions/), depending upon unique challenges/interests

      **VP Ops** - Keeping the business running efficiently.  Uptime, recovery are important.  The role is typically bonused/compensated on uptime and SLAs back to the business.
      1. Value Prop:
          - GitLab’s single application that supports the entire DevOps lifecycle is going to be important for this role.  Shifting left monitoring, testing and security earlier in the SDLC is valuable to the VP Ops to reduce the risk of downtime in production.  Auto Devops will be of interest here as well.
      1. Resources:
            - [GitLab Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
            - [GitLab home page](https://about.gitlab.com)
            - [ROI](https://about.gitlab.com/roi/replace/)
            - [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/)
            - [Auto DevOps](https://about.gitlab.com/auto-devops/)
            - [Geographically distributed teams](https://about.gitlab.com/features/gitlab-geo/)
            - [High availability development](https://about.gitlab.com/high-availability/)
            - [DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops)
            - [Product page](https://about.gitlab.com/product/)
            - [Comparison page](https://about.gitlab.com/comparison/)
            - [Solutions page](https://about.gitlab.com/solutions/), depending upon unique challenges/interests
            - [Security Deck](https://docs.google.com/presentation/d/1lNr9pz7axLlN7uw7Wkwi_FYMuEh4F4QzPaoJLfReGFk/edit#slide=id.g2823c3f9ca_0_9)
            - [Regulatory Compliance](https://about.gitlab.com/solutions/compliance)

      **VP/Dir DevOps or Enterprise Architect** - VP/Director of DevOps is a fairly new role. Enterprise Architect role is expanding with more and more control.  Leading the execution of the transformation. Ensures the business and technology are in alignment. Focuses on best practices and processes as well as assists with documentation. Evangelist of DevOps best practices.
      1. GitLab persona reference: [Director DevOps video](https://www.youtube.com/watch?v=qyELotxsQzY), [Director DevOps slide deck](https://docs.google.com/presentation/d/1x-XUhAXkZxl8ZGe4ze9qcWMmrWITc7es1fYNY_OHQQA/)
      1. Value Prop:
          - GitLab’s single application enables more rapid development (see GitLab stats as an example) to meet time-to-market business challenges.
          - Unite workflows around a common tool to reduce friction and cost of IT.
          - Reduce or eliminate your costly DevOps tool chain (or avoid this investment in one).
      1. Resources:
            - [GitLab Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
            - [GitLab home page](https://about.gitlab.com)
            - [ROI](https://about.gitlab.com/roi/replace/)
            - [CI/CD](https://about.gitlab.com/features/gitlab-ci-cd/)
            - [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/)
            - [Cloud Transformation](https://about.gitlab.com/solutions/cloud-native/)
            - [Google Cloud Platform](https://about.gitlab.com/google-cloud-platform/)
            - [AWS](https://about.gitlab.com/aws/)
            - [Software-defined Infrastructure](https://about.gitlab.com/solutions/no-ticket-provisioning/)
            - [Geographically distributed teams](https://about.gitlab.com/features/gitlab-geo/)
            - [DevSecOps](https://about.gitlab.com/solutions/dev-sec-ops)
            - [Product page](https://about.gitlab.com/product/)
            - [Comparison page](https://about.gitlab.com/comparison/)
            - [Solutions page](https://about.gitlab.com/solutions/), depending upon unique challenges/interests

      **VP Security** - Large enterprises will have this but others go straight to Manager of Security.
      1. Value Prop and Resources = see CISO  


      **VP Shared Svcs** - Common services for IT (Project Mgt, Portfolio Mgt, Resource Mgt, perhaps QA, etc) - for enterprises with microservices model
      1. Value Prop - Project & [Portfolio Mgmt](https://about.gitlab.com/solutions/portfolio-management/), Testing (QA)
      1. Resources - future vision


      **VP PMO / EPMO** - Gathering and managing business projects/demand, execution, on-time, on budget
      1. Value Prop - Issues, issue boards, etc but of interest to his/her Directors
      1. Resources - future vision


1. **Directors**
      1. Service Mgt - Service Desk, Incident mgt, responding to outages, recovery
      1. Release Mgt - managing change and configuration of the IT systems
      1. DevOps? - Transformational role or maybe re-branded from 'release mgt'
      1. "Business XYZ". (they own the applications for a specific business unit.  Manage demand, dev, delivery
      1. Portfolio Mgr/Director  Manages a business unit's portfolio of projects/initiatives.  on-time, on budget
      1. Testing / QA - Quality and Testing - plan testing, provide resources, execute tests, track quality
      1. Security - Establish policies, procedures, and processes to secure IT

1. **Managers**
      1. Dev Team Manager - leads development team for a specific business function/system
      1. QA Manager - Test Leads, Automated Testing, Performance Testing, Test Environments, Test Data
      1. Project Manager - plan, organize, and execute projects so they deliver on time, on budget
      1. DevOps - Probably 'release manager'
      1. Release - Running day to day release management, documenting changes, ensuring approvals, etc
      1. Incident / Service Desk - Responding to problems/incidents from end users, restoring service
      1. Configuration - ensuring the configuration of environments is documented, controlled and managed
