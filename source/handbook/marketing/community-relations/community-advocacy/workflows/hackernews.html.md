---
layout: markdown_page
title: "Hacker News"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

The [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel tracks mentions of GitLab on HackerNews. It is a dedicated one, so that Community Advocates enable channel notifications and can respond to them as soon as possible.

## Workflow

![Hacker News channel workflow](/images/handbook/marketing/community-relations/hn-mentions.png)

1. React to notifications and go to the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel when you see them them appear
1. Read every new message in the channel and make a decision for each one: does it need a response?
1. Respond to the message if necessary, or [involve an expert](/handbook/marketing/community-relations/community-advocacy/#involving-experts) using the template
1. Ping `@sytse` in the `#community-relations` Slack channel if you judge his input is required, or in case of doubt
1. Add a checkmark (`:heavy_check_mark:`) to the message on the Slack channel

Every message should have a checkmark to indicate that it was processed: either someone responded to the mention or decided that it didn't need a response.
{: .alert .alert-warning}

## Best practices

When responding to a post about GitLab on Hacker News:

* Post a link to the thread in the `#community-relations` Slack channel and ping other `@advocates`.
* Don't post answers that are almost the same, link to the original one instead.
* Address multi-faceted comments by breaking them down and using points, numbering and quoting.
* When someone posts a HackerNews thread link, monitor that thread manually. Don't wait for the notifications in the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) channel, because sometimes they're delayed by a few hours.

Always bear in mind the [social media guidelines for Hacker News](/handbook/marketing/social-media-guidelines/#hacker-news) in all your interactions with the site.
{: .alert .alert-info}

## Automation

These mentions are piped to the [#hn-mentions](https://gitlab.slack.com/messages/hn-mentions) Slack channel by [Zapier](https://zapier.com).
