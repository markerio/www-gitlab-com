---
layout: markdown_page
title: Product Vision - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Manage in 2019 and beyond. If you'd like to discuss this vision directly with the product manager for Manage, feel free to reach out to Jeremy Watson via [e-mail](mailto:jwatson@gitlab.com), [Twitter](https://twitter.com/gitJeremy), or by [scheduling a video call](https://calendly.com/jeremy_/gitlab-product-chat).

## Overview

For administrators and executives, the process of management is always on. It extends to managing people, money, and risk; when the stakes are high, these stakeholders demand an experience and feature set that makes them feel in control. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to make software work for them.

Not only do we want to fulfill those fundamental needs, we want to give you the freedom to work in new and powerful ways. We aspire to answer questions managers didn't know they had, and to automate away the mundane.

Manage's role in GitLab is to **help organizations prosper with configuration and analytics that enables them to work more efficiently**. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

<!--Managing the DevOps lifecycle requires configuration, control, and insightful data to power
intelligent decisionmaking. The process of managing the business is always on.

This involves traditional tools like authentication and authorization,
but goes beyond into sophisticated analytics and workflow solutions that take advantage of
GitLab's single-application approach to the DevOps lifecycle. We want to help organizations
prosper with configuration and analytics that enables them to work more efficiently.
-->

## Problems we're solving

We’re realizing this vision in 2019 by delivering more powerful insights, giving you the power to automate custom workflows, and iterating on must-have features for complex organizations.

* How do we tell users what they need to know?

As instances thrive, the amount of information flowing through GitLab grows exponentially. An administrator’s job quickly becomes more reliant on automation and tools to help them stay reactive (I need to respond to an urgent request for information) and proactive (tell me about areas of risk).

Part of telling administrators what they need to know is about keeping you well-informed. In 2019, we want to allow instances to be less reactive and more proactive about threat assessment; we’ll be introducing automated [behavioral monitoring](https://gitlab.com/groups/gitlab-org/-/epics/259) to audit logs. In the future, administrators will be able to set up [listeners for events](https://gitlab.com/gitlab-org/gitlab-ee/issues/6762) and [create issues](https://gitlab.com/gitlab-org/gitlab-ee/issues/7624) when these pre-defined alert conditions are met.

When it’s time to dig deeper into an event, we want to ensure that changes in GitLab are traceable and transparent. After code gets merged, it may involve a host of individuals, commits, and objects - while we make it easy to go from idea to production, tracing that history back should also be a cinch. Since the heart of code change in GitLab is the merge request, we’ll add the ability to see the a deep history - including the issues, people, and commits - that led to the change.

By building monitoring and traceability deep into the application, our goal is to let GitLab thrive in any regulated environment.

Keeping you informed is only a part of what administrators need; we also want to tell you something insightful and new about how you’re shipping software. We’ll offer powerful [code analytics](https://gitlab.com/groups/gitlab-org/-/epics/368) at the project level that tells owners and maintainers about hotspots in your code that churn frequently. After these have been identified, we’ll make it easy to open an issue to track the subsequent refactor.

* How can we make working in GitLab effortless?

GitLab’s [single application](https://about.gitlab.com/handbook/product/#single-application) approach to DevOps puts the entire software development lifecycle in a single place. As a result, users don’t have to stitch together an overly-fragmented toolchain - and we want to go deeper in on that advantage.

GitLab is built to be inherently flexible, as organizations have a variety of different workflows and structures. Instead of strictly representing these workflows with manual handoffs, we want to automate the processes that define your organization. In the same way that you’re able to define a CI pipeline, we want to bring this concept to how you use the rest of the application - end-to-end.

In 2019, we’ll make this happen with [Automations](https://gitlab.com/groups/gitlab-org/-/epics/218). Instances will be able to define complex workflows with conditional logic, vastly reducing time spent on the transactional. Instead of manually applying labels to new issues, for example, you’ll be able to automate your workflow to automatically apply a label like “Waiting for Triage”.

Conversely, organizations may want to enforce their workflows by defining events that should’ve already taken place. Like automations, we should be able to define - and enforce - these restrictions in code, ensuring compliance and peace-of-mind.

We’re calling these restrictions [Policies](https://gitlab.com/groups/gitlab-org/-/epics/366), and we’re adding them as a mechanism to define prerequisites for certain actions in GitLab. You’ll be able to [restrict certain actions](https://gitlab.com/gitlab-org/gitlab-ee/issues/7626) - creating an issue, for example - to certain users that have met a custom requirement that you’ve defined.

* How can we continue to support large, complex instances?

As GitLab continues to grow, we want to continue to iterate and improve on existing features. This is especially true of features that help large organizations thrive in GitLab.

We’re continuing to improve on authentication within GitLab, which is of critical importance for managing users at scale. We’re continuing to build out Group SAML for GitLab.com by automating membership and permissions management. We’re also improving OAuth by allowing you to programmatically manage tokens.

Finally, we’re excited to give instances more control and power over how they manage spending. You’ll be able to clearly understand how your instance’s license is being used, with granular control over seats. Alongside making billing easier to understand than ever, we’re also improving our billing portal to give you the power to self-serve changes to your subscription.

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Manage can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=devops%3Amanage), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

You can see further details on the prioritization and development process on the [page for the Manage team](https://about.gitlab.com/handbook/product/categories/manage/).

## Our plans

We couldn’t be more excited to make GitLab easier than ever to manage. In 2019, we’ll do this by building out powerful configuration that reflects your unique needs and providing you with insightful analytics that helps you move faster than ever.

We’ll keep iterating on these concepts well into 2020, and continue adding more powerful analytics and collaboration features to help you work seamlessly in GitLab.

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
